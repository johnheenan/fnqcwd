---
title: "Basic Workflow"
description: "A disorganised collection of helpful howto information consistent with the goals of the project."
lead: "A disorganised collection of helpful howto information consistent with the goals of the project."
date: 2021-11-06T08:49:31+00:00
lastmod: 2021-11-06T08:49:31+00:00
draft: false
images: []
menu:
  docs:
    parent: "help"
weight: 632
toc: true
---

## YouTube video

[Updating a Hugo web site for Firebase](https://www.youtube.com/watch?v=Zu-3XA-NVLs)

{{< youtube Zu-3XA-NVLs >}}


## Basic day to day workflow

We will assume a hugo framework is being used, tools have been setup and you are working on an established website where a directory setup defines behaviour of `git` and `firebase` commands in that directory. Nemo is the name of the file explorer in Cinnamon (like File Explorer in Windows).

1. Right click on work directory in Nemo and click on 'Open in Terminal'
1. Open your favourite editor, such as VSC (Visual Studio Code) with command `code .`. Note the full stop at end of command. This is important.
1. In a new or same terminal at same directory run command  `hugo server` (or `npm run server` if hugo doks theme) to start a local server
1. `Ctrl + Single Click` on <http://localhost:1313/> in terminal to view on local server in Firefox or refresh existing Firefox tab. Note `Ctrl + C` stops server.
1. Start or continue editing your framework files, such as Hugo markdown files
1. Make sure you save your changes in editor to **view live changes on local server**. Maybe leave 'Auto Save' on in VSC.
1. When finished stop local server in its terminal with `Ctrl + C`. You may occasionally need to stop and restart if there are problems with the local server
1. To build web site use `hugo` (or `npm run build` if hugo doks theme)
1. To upload web site changes run command `firebase deploy`
1. Check everything looks OK. Make further changes if necessary
1. When finished run the following commands to upload changes to GitLab or GitHub to keep records and for collaboration
   1. Command `git add .` (Note the full stop at end)
   1. Command `git commit -m 'Your one line message about the changes you have just made'`. Note `git commit` on its own will start an editor and allow you to leave a multi line message.
   1. Command `git pull --rebase` is a good habit to get into and will pull in other peoples changes if collaborating. If there is conflict then you will need to fix and alter workflows to avoid happening frequently. For example running `git pull --rebase` earlier before you start editing
   1. Command `git push` to update repository
   2. Git is complex and does a lot behind the scenes. It is not sound practice to put all these git commands into a single command file and run it.


Whenever a file is changed (for example by saving an editor file or adding/removing an image file) the local web server updates. Sometimes it is less disruptive to temporarily stop the local web server and leave off until bulk changes are finished.

If 'Auto Save' is on in VSC and the local web server is running, then updates can be viewed immediately.

If the local web server becomes out of sync then stop and restart it.

## Command summaries


### Start and stop local web server

```
npm run server # or hugo server
#Ctrl + Single Click on URL (http://localhost:1313) to view in web browser
#Ctrl + C to stop
```

<img src="/animatedgif/fnqcwd_local_web_server.gif" alt="Start and stop local web server video" width="820"/>

See [YouTube video above](#youtube-video) for more context



### Edit and deploy

```
# Edit files and optionally test locally with npm run server or hugo server
npm run build # or hugo
firebase deploy
```

<img src="/animatedgif/fnqcwd_web_build_deploy.gif" alt="Edit and Deploy video" width="807.5"/>

See [YouTube video above](#youtube-video) for more context


### Upload changes to collaboration repository

```
git add .
git commit -m 'your commit message'
git pull --rebase
git push
```

THIS ALSO BACKUPS YOUR WORK. THIS ALONE WILL MAKE YOU VERY GRATEFUL SOME DAY!

<img src="/animatedgif/fnqcwd_git_workflow.gif" alt="Update GitHub or GitLab video" width="807.5"/>

See [YouTube video above](#youtube-video) for more context

---
title: "Git Quick Tips"
description: "A disorganised collection of helpful howto information consistent with the goals of the project."
lead: "A disorganised collection of helpful howto information consistent with the goals of the project."
date: 2021-11-06T08:49:31+00:00
lastmod: 2021-11-06T08:49:31+00:00
draft: false
images: []
menu:
  docs:
    parent: "help"
weight: 642
toc: true
---


## Git Quick Tips (Incomplete)


### Get an up to date version of Git  on Cinnamon
```
sudo add-apt-repository ppa:git-core/ppa -y
sudo apt-get update
sudo apt-get install git -y
git --version
```
### Using a Git GUI

Lots of choices. Good for an overall view. VSC has a 'Source Control' view built in that is useful that shows git file differences clearly.

A Git GUI is less ideal to use for operations than the command line.

There is a native Git GUI called gitk.

On Windows it is probably already installed and it is started as `gitk` in the same directory git is used as a command.

On Linux you can start it in a directory as `gitk &`. The  `&` frees the shell for other commands. If gitk is not installed then you can install it with `sudo apt install gitk`.


### Stop Git asking repeatedly for credentials

```git config --global credential.helper store```

Better to use SSH. See <https://stackoverflow.com/questions/35942754/how-can-i-save-username-and-password-in-git>


### Using CI/CD

CI/CD is Continuous Integration/Continuous Deployment and involves using what can be a complex chain (including tests) to go from pushing a change with Git to automatic deployment of production or release software, in this case the website, bypassing a `firbase depoly` manual command.

We do not recommend full CI/CD for small one or two people teams who make infrequent updates and who are not familiar with the processes and what CI/CD does.

On the other hand if you deploy your website to GitHub or GitLab instead of Firebase then a push needs to trigger a deployment. There are guides for this at <https://pages.github.com/> (GitHub) and <https://docs.gitlab.com/ee/user/project/pages/> (GitLab).

#### CI/CD for GitHub with Firebase
There are examples of two firebase hosting GitHub Actions at <https://github.com/marketplace/actions/deploy-to-firebase-hosting>, one for previews if a PR (pull request) and one for production if a merge.

#### CI/CD For GitLab with Firebase
Have a look at <https://about.gitlab.com/blog/2020/03/16/gitlab-ci-cd-with-firebase/>



### How to upload to GitLab  a new site from  the Hugo  doks child theme, KEEPING commit history

Assume repository `mysite` does not exist or has been deleted and `myusername` is your username on gitlab
```
git clone https://github.com/h-enk/doks-child-theme.git mysite
cd mysite
git remote rename origin old-origin
git remote add origin https://gitlab.com/myusername/mysite.git
git push -u origin --all
git push -u origin --tags
```
Change access to public if desired


### How to upload to GitLab  a new site from  the Hugo  doks child theme, RESTARTING commit history

Assume repository `mysite` does not exist or has been deleted and `myusername` is your username on gitlab
```
git clone https://github.com/h-enk/doks-child-theme.git mysite
cd mysite
rm -rf ./.git
git init --initial-branch=main
git remote add origin https://gitlab.com/myusername/mysite.git
git add .
git commit -m "Initial commit"
git push -u origin main
```
Change access to public if desired







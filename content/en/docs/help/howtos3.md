---
title: "Firebase Quick Tips"
description: "A disorganised collection of helpful howto information consistent with the goals of the project."
lead: "A disorganised collection of helpful howto information consistent with the goals of the project."
date: 2021-11-06T08:49:31+00:00
lastmod: 2021-11-06T08:49:31+00:00
draft: false
images: []
menu:
  docs:
    parent: "help"
weight: 643
toc: true
---






## Firebase Quick Tips


### How to setup firebase to just simply run 'firebase deploy' on a rebuilt website

You need to have firebase-tools installed. Please see [Basic Installation]({{< relref "basic-installation" >}}) if necessary.

Only use the `firebase init` command as a last resort to setup a project. It involves being invited to make complicated choices, the scope of which are well beyond what is required to get going. The `firebase init` command is not even necessary to setup a basic project from scratch. If you are unsure of how to proceed it is best to have someone set up a project remotely for you so you can get going easily

This example assumes you do not have a multi site project and your website is built in the public directory

1. Clone project with git and open a terminal in cloned directory.
1. Check you are already logged into firebase with `firebase login`. If not complete login process.
1. Check your logon  has access to project you want to update (listed in .firebaserc)  with command `firebase projects:list`. In the 'Project ID' column the current project will have '(current)' in it.
1. This is enough to get going. If not then`firebase init` can be used but it is best avoided except for experienced users and is not needed if the project has been setup for you already.
1. If there are no `.firebaserc` and no `firebase.json`  files but your project ID, such as  `mysite-6883c` exists with a single default hosting site you can use `firebase init` (best avoided), ask someone else for help or you can use template files such as:
1. ` .firabaserc` file for project aliases
```json
{
  "projects": {
    "default": "mysite-6883c"
  }
}

```
5.  `firebase.json` file for project configuration, assuming rebuilt website is in the local public directory
```
{
 "hosting": {
    "public": "public",
    "ignore": [
      "firebase.json",
      ...
    ]
  }
}
````
`hugo` or `npm run build` rebulds the website in the local public directory.

6. Check there is a `.firebase` line to `.gitignore` file to ensure the firebase cache directory is not uploaded to GitLab when your repository is updated.
7. Upload with command `firebase deploy`.


### You want to use 'firebase init' because setup is incomplete and you are not using a multi site firebase project

1. Use an existing project or create a new project in firebase with a single hosted site, say the project name is mysite-3333. It does not matter if the hosting site is new and has no content. For a project with a single hosting site, the hosting site can be the same name as the project name.
1. Get your project working locally first, including with Git (such as with `git clone` command)
1. Make sure the directory for upload to the web is called public and is in the root directory of the project
1. In the root directory of the project delete or rename .firebaserc and firebase.json files
1. **In the root directory run the following command: `firebase init hosting`. Make sure you add the word hosting to shorten your choices**
1. Press enter if asked 'Ready to proceed'
1. If asked to choose an account, then use arrow keys to choose one and then press enter.
1. Press enter to 'Use an existing project'
1. Use arrow keys to choose the 'mysite-3333' project (or whatever) with enter
1. Keep pressing enter until the command finishes (taking the usual default choices, such as 'No')


### Is there an easier way to above?

Yes. For simple sites carry out steps 1. to 3. and just copy over working equivalent firebase.json and .firebaserc files, similiar to above. Change one word in the .firebaserc file. If the other site name is "mysite-2222" then change this name to "mysite-3333" (or whatever). You might like to copy over .gitignore also.

Note replace the ... in firebase.json file with two the lines they represents. These two lines cannot be included on this page because the double * characters in a code section interfere with the search functionality of this web site.


### What about domain names and linking to hosting site.

The easy way is to ask someone who has experience and can get it done quickly. Even if you take a ton of notes watching someone do it it will still be hard doing it on your own the next time. The hardest way is try it on your own, thinking you will get it done quickly and without frustration.

If using firebase then it may be easier if you use 'Google Domains' nameservers. This is not advised as it forces excessive vendor lock in.


### What about secure https sites?

You have got it lucky. It is now automatic, unless you want to deploy on non managed servers (like your own server).



### How to update firebase-tools

Occassionally firebase will state an update is available and to use `npm install -g firebase-tools`.

If you installed firebase tools with `brew install firebase-cli` then use this same command to update instead.

If you installed firebase tools on Cinnamon with `sudo -i npm i -g firebase-tools` then use this same command to update instead.


### To deploy a preview on firebase

Instead of
```
firebase deploy
```
use
```
firebase hosting:channel:deploy preview_name
```



### Target another (non default) hosted site in a firebase project

There is an easier way below

See
* <https://stackoverflow.com/questions/52149583/firebase-hosting-multiple-sites-configure-firebaserc-file>
* <https://firebase.google.com/docs/cli/targets>
* <https://dev.to/forcommunity/how-to-deploy-multiple-sites-to-firebase-hosting-38l3>

Simple example for webapp fnqcw1 in project fnqcwd.
Change `firebase.json`
```json
{
 "hosting": {
    "public": "public",
    "ignore": [
      "firebase.json",
      ...
    ]
  }
}
```
to
```json
{
 "hosting": {
    "target": "fnqcw1",
    "public": "public",
    "ignore": [
      "firebase.json",
      ...
    ]
  }
}
```
and run `firebase target:apply hosting fnqcw1 fnqcw1` to automatically edit `.firebaserc`


### Easier way to include another non default site in a firebase project

Suppose for the default project with default webapp fnqcwd `.firebaserc` is
```
{
  "projects": {
    "default": "fnqcwd"
  }
}

```
and `firebase.json` is
```
{
  "hosting": {
    "site": "fnqcwd",
    "public": "public",
    "ignore": [
      "firebase.json",
      ...
    ]
  }
}

```
Then for a non default web app fnqcw1 the following works with no change to `.firebaserc`
```
{
  "projects": {
    "default": "fnqcwd"
  }
}

```
and a single line change with "site" to `firebase.json` is
```
{
  "hosting": {
    "site": "fnqcw1",
    "public": "public",
    "ignore": [
      "firebase.json",
      ...
    ]
  }
}

```

No need to run `firebase target:apply hosting ...`




### Changing over from Brew to nvm for Node/npm and Firebase-tools

Please see <https://github.com/nvm-sh/nvm> for information on installing latest version of nvm and on using nvm.

```
brew uninstall firebase-cli
brew uninstall node
cd
rm -rf .npm
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
exit
```

Access a new shell
```
nvm install --lts  # note this is n v m, not n p m
npm install -g firebase-tools  # note back to n p m, not n v m
corepack enable # run once if you want to use yarn command

```
Your previous firebase login will still be valid.

In addition you will be able to use `yarn`, if you wish, simply by running `corepack enable` once


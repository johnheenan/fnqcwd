---
title: "Introduction"
description: "To Be filled in."
lead: "To be filled in."
date: 2021-11-14T08:48:57+00:00
lastmod: 2021-11-14T08:48:57+00:00
draft: false
images: []
menu:
  docs:
    parent: "prologue"
weight: 100
toc: true
---

## What is here

1. [An announcement of a project in an exploratory stage.]({{< relref "fnqcwd-project" >}})
1. [A disorganised collection of helpful information consistent with the goals of the project]({{< relref "howtos1" >}})
1. Examples of barely modified theme sites, using the same theme this web site used, are at:
   1. <https://fnqcw1.zgus.com/>
   1. <https://fnqcw2.zgus.com/>
   1. <https://fnqcw3.zgus.com/>



## Most Immediate Support Resource Needs

If the project goes ahead then these are the most pressing support resources that need to be developed:

1. Themes (from beginner to advanced levels with ability to migrate up to more advanced themes)
1. Step-by-step videos for beginners
1. An approval process for a community to be accepted for support, if they apply. We would anticipate such an application and process would be largely through a flexible personal process rather than through a formal written process.
1. Preprepared laptops that can be donated, if necessary


## YouTube Channel

[FNQ WebDev Support Playlist](https://www.youtube.com/playlist?list=PLcNHA18ZbSKPKdKlwVlcfvFw8-BF_cHFY)


## Themes


If the project goes ahead then a priority would be curate, develop and recommend website themes suitable for beginner and more advanced use in lines with the goals of the project.


## Beginners And Collaboration

If you are interested in helping build and maintain a website for your community, but consider your self a beginner, then we hope step-by-step videos planned to be made will help, if the project goes ahead.

You could start by working on an empty theme or you could start your own corner on an established website through collaboration and take on more responsibilities as you gain more experience and work with sources.


## Who decides content?

For continued support from us, the community alone decides content. We hope over time there will be sufficient community sites to act as guidelines through example.

### Can the person or people controlling the laptop also write and decide content?


For continued support from us, only if the community specifically allows it and this responsibility is accepted. If the community does not specifically offer, or if offered but is not specifically accepted, then the person or people controlling the laptop can only take instructions on what existing content to use or will be provided, not create or write it. It should be remembered the job of a laptop controller can be time consuming and they should not be distracted by what they do not need to do. Also the time of a laptop controller is better spent improving their skills rather than creating content. In addition a laptop controller may be seeking to improve their skills for a specific purpose, such as for a job market. In that case it is unfair to distract them by expecting them to create content instead of the quicker task of just presenting content.

By the person or people 'controlling the laptop' we mean the person or people who places content on the website, not the content authors. An accurate but confusing term is a webadmin or web administrator in the sense that an administrator carries out assigned tasks, not creates them. They carry out tasks assigned by the community, such as to present specific content.

However such tasks will usually involve editing both for clearer meaning and for improved presentation, as long as the original meaning and intention is retained. This means a 'laptop controller' needs a high level of language and communication skills, often a lot better than the person or people providing content. **Those providing content need confidence that a 'laptop controller' will not only faithfully present their content or message but also improve on its presentation**.  A laptop controller should not regard a task to improve presentation as trivial. Being able to improve presentation is a valuable skill that requires practice.

This means a very young person can be responsible for the laptop and placing instructed content on the website. They cannot and should not be held responsible for the content or for deciding content.

So, it should not be assumed someone who is prepared to control and look after the laptop should also be prepared to write original content representing the community. They may of course do so but this must be by a specific offer of a community representative and be by specific agreement of the laptop controller. If the laptop controller does agree to write content they must agree to represent the community, not their own interests.


## Get started

Not ready yet.

### Tutorial

{{< alert icon="👉" text="The Tutorial is intended for novice to intermediate users." />}}

Step-by-step instructions on how to start a new FNQCWD project. Not ready yet.

### Quick Start

{{< alert icon="👉" text="The Quick Start is intended for intermediate to advanced users." />}}

One page summary of how to start a new FNQCWD project. Not Ready yet.

## Go further

Recipes, Reference Guides, Extensions, and Showcase.

### Recipes

Get instructions on how to accomplish common tasks with FNQCWD. Not ready yet.

### Reference Guides

Learn how to customize FNQCWD supported templates to fully make it your own. Not ready yet.

### Extensions

Get instructions on how to add even more to FNQCWD. Not ready yet.

### Showcase

See what others have build with FNQCWD. Not ready yet.

## Contributing

Find out how to contribute. Not ready yet.

## Help

Get help on FNQCWD. Not ready yet.


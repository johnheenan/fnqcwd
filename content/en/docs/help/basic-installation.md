---
title: "Basic Installation"
description: "A disorganised collection of helpful howto information consistent with the goals of the project."
lead: "A disorganised collection of helpful howto information consistent with the goals of the project."
date: 2021-11-06T08:49:31+00:00
lastmod: 2021-11-06T08:49:31+00:00
draft: false
images: []
menu:
  docs:
    parent: "help"
weight: 631
toc: true
---


## Hardware requirements for support with FNQ WebDev

Using Linux commands:

1. At least 4GB RAM. First number on second line from `free -h`
1. At least 100GB disk storage. First number with G from `df -h | grep /$`
1. x86_64 processor. Absolute requirement if using homebrew with Cinnamon. `uname -p`


## Base OS requirement for support with FNQ WebDev
1. Cinnamon Linux Mint version 20 or higher `cat /etc/linuxmint/info | grep -i title`


## Base install of webadmin tools after clean install of Cinnamon Linux Mint

This assumes you already have been granted write access to both a suitable firebase project and git repository. The example here is not for full CI/CD but is similar.

You can get faster updates in Mint by modifying Update Manager with Edit, Software Sources, click on Main and Base and choosing fastest Australian flagged mirrors at top.

* Do a standard cinnamon linux mint install unto a PC or VM. Include codecs when asked
* Reboot
* Install language packs if asked
* Start 'Update Manager
  * Apply update to 'Update Manager' if asked
  * Use local mirrors when asked or use them even if not asked
  * For Australia only: click on Main (uma) box and choose AARNet near top with Australian flag (Avoid OFFRATEL LAGOON and EvoWide CDN). Click on Apply.
  * For Australia only: click on Base (focal) and choose Australian flag mirror near top with aarnet in it. Click on Apply.
  * Click on OK. The reason for choosing AARNET, even if not the fastest, is that the AARNET mirror is complete and reliable.
* Open a Terminal (for example use `Ctrl  + Alt + T`) and install Homebrew (brew) with hugo and firebase-cli
```
cd
sudo apt update
sudo apt upgrade
sudo apt-get install build-essential procps curl file git
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"' >> $HOME/.profile
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
brew install hugo
brew install firebase-cli

```
Following install of Homebrew, `brew upgrade` will upgrade installed packages.

* Install VSCode
```
cd
sudo apt update
sudo apt install apt-transport-https
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sudo install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
rm microsoft.gpg
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
sudo apt update
sudo apt install code

```

* Specific only to fnqcwd website project. Alter to one you have write access for both relevant firebase project and for the repository.
```
cd
mkdir fnqcw
cd fnqcw
git clone https://gitlab.com/johnheenan/fnqcwd.git
cd fnqcwd
firebase login # required once and launches firefox to login
firebase projects:list # make sure fnqcwd is listed and is the current project
npm install # not required for a standard hugo theme and if required then not required regularly (such as for a hugo doks theme)
code . # Check VSCode is working. Notice the dot. Use 'File, Autosave'

```

* Setup git globally for writing commits
```
  git config --global user.email "you@example.com"
  git config --global user.name "Your Name"
```

* Optionally, setup git globally to store repository logon credentials. You need to be aware there are different types of logon that do not necessarily share the same password or credentials. After you logon once, your credentials are stored for reuse in plain text. **For GitHub you can no longer use your GitHub web browser login password for CLI (Command Line Interface) or API (Application Program Interface) login, such as with this method**. You can instead store a personal access tokens (PAT). Please see '[Create a PAT](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token)' for GitHub. There are also other login methods that do not involve storing a token or password in plain text.
```
git config --global credential.helper store
```

* Again: for GitLab you can use your GitLab password as your stored credentials. For GitHub you must use a PAT as per above.

* Leave out --global in three commands above to store information in a per directory project basis.
* For --global flag, credentials are stored in plain text in `~/.git-credentials` and config is stored in `~/.gitconfig`.


* Continue with [Basic Workflow]({{< relref "basic-workflow" >}})

Avoid using timeshift unless you are clear about the consequences.


## Alternative installs of webadmin tools with Linux, Windows and macOS

Some notes for various OS, including Linux (Debian or Ubuntu based), Windows WSL and macOS


### Homebrew (or brew) for Cinnamon and WSL Debian
Can be used to install an up to date version of Hugo below and other software


To install Homebrew (do not execute the following commands as root user):

```
sudo apt update # if not recent
sudo apt upgrade # if not recent
sudo apt install build-essential procps curl file git
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
# The following commands or equivalents appear as output from the above command after a lengthy process
# Next two commands add Homebrew to your PATH
echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"' >> $HOME/.profile
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
brew install gcc  #can be left out

```
Documentation on brew at <https://docs.brew.sh>. Run ```brew help``` to get started with brew


### Homebrew (or brew) for other Linux, other WSL Linux and for macOS

See <https://brew.sh/>.

May still work with command below but with different follow ups:
```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

### Hugo

Please do not install hugo with command below. It will out of date
```
sudo apt install hugo
```

Hugo runs from a single file binary. If you are not comfortable installing a single file binary from <https://github.com/gohugoio/hugo/releases> or installing from source then for Cinnamon Linux Mint you can use or Homebrew, as mentioned (for Linux) at <https://gohugo.io/getting-started/installing/#homebrew-linux>


Install hugo or install an update of hugo with homebrew:
```
brew install hugo
```


### Node/npm
If you use Homebrew (brew) then skip this step.

Please do not install node/npm with command below. It will be years out of date
```
sudo apt install nodejs
```

If you install homebrew and use homebrew to install firebase-cli then node/npm gets installed as a dependency and you can also update firebase without node/npm!


node v14x is a good choice. See <https://github.com/nodesource/distributions#debinstall>
```
sudo apt update
sudo apt upgrade -y
sudo apt install curl build-essential
curl -fsSL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt-get install -y nodejs

```

### Firebase-tools (firebase-cli)
An alternative to below that avoids using `sudo -i` for installing and updating is to use
```
brew install firebase-cli

```
This installs gcc and node/npm as dependencies!


Once firebase is installed you can run `firebase login`. It will take you to a web browser to complete the login.

Once logged in you can run `firebase projects:list` and you can run updates to the web site with a bit more set up.


To install firebase-cli with node/npm instead of brew then after installation of Node/npm as per above:
```
sudo -i npm install -g firebase-tools
```
If you do not use brew and if you use the -g flag (for global) with npm, you must use `sudo -i` or run as root user some other way.  The `-i` is important, including when updating with same command



### Using nvm instead of brew to install Node/npm and Firebase-tools

nvm allows different versions of node to be easily used. Also for Linux and WSL it avoids having to prepend `sudo -i` to `npm install -g` commands

nvm is also the recommended way by Google to installs Firebase tools with `npm install -g firebase-tools`.

However if you just plan to stick to static websites we recommend you use brew instead to install node and firebase tools.

Please see <https://github.com/nvm-sh/nvm> for further information on installing and using nvm.

There is a section on changing over from Brew to nvm for Node/npm and Firebase-tools at [Firebase Quick Tips]({{< relref "howtos3/#changing-over-from-brew-to-nvm-for-nodenpm-and-firebase-tools" >}}). Also minimal setup instructions are included.


### Discord

A better way to get Discord to work with Mint and voice may work with it. Based on <https://linuxconfig.org/how-to-install-discord-on-ubuntu-20-04-focal-fossa-linux>
Works a lot faster that flatpak version. Current Mint Cinnamon is built on Ubuntu 20.04
```
sudo apt update
sudo apt install gdebi-core wget
cd
wget -O ./discord.deb "https://discordapp.com/api/download?platform=linux&format=deb"
sudo gdebi ./discord.deb
rm ./discord.deb
```
Find by typing Discord in LM icon at bottom left. You can start discord from console but console won't exit unless you use tricks.



---
title: "FNQCWD Project"
description: "FNQCWD - Far North Queensland Communities WebDev support - Just a working name for a project in an exploratory stage that may or may not go ahead"
lead: "FNQCWD - Far North Queensland Communities WebDev support - Just a working name for a project in an exploratory stage that may or may not go ahead"
date: 2021-11-14T09:19:42+01:00
lastmod: 2021-11-14T09:19:42+01:00
draft: false
weight: 50
images: [""]
contributors: ["John Heenan"]
---

The FNQCWD name will change if the project goes ahead.

The purpose of the project is to provide support to communities in Far North Queensland Australia to represent themselves as they see fit.

There are agreed terms of reference for the project but they are not being revealed yet.

The support protocols are in development. There is nothing stopping anyone else anywhere using this project as a resource and even contributing to it but if you want to support other communities you will need to find someone to work with who is well recognised both within and outside those communities, and who understands the realities, limitations, risks and challenges.

Support for community webdev in FNQ is intended to be highly practical. Because of this we need the following:

1. A way to integrate such support into existing support structures and flows. Such support structures and flows are highly personal and involve building trust over long time scales, particularly where there is a long history of mistrust.
2. A technical support structure that makes and sticks to recommendations based on:
   1. Very low capital cost and ongoing cost of hardware, software and service vendors, that must run very efficiently, be trouble free  and be highly fit for purpose
   2. For example, operating systems and software that runs comfortably on old hardware, such as older laptops with at least 4GB RAM.
   3. It is expected mobile broadband with expensive and slow bandwidth will be extensively used as well as sluggish satellite broadband, so another example is being able to minimise upload and download traffic and being able to control when software updates occur. This applies to both webdevs and to users.
   4. Counter-intuitively, but consistent with contemporary practices, ease of support preferring efficiency and speed of command line support over ambiguity, wordiness and slow progress of windows point and click support
   5. Low security risk and ease of keeping security up to date
   6. Ease of collaboration and passing on knowledge
   7. Choice of software that is likely to be still around many years form now
   8. Choice of vendors that are likely to be still around many years form now

For the technical reasons above this means the project is likely to standardise on only supporting Cinnamon Linux Mint OS on laptops with 4GB or over of RAM, support static websites only, support GitLab or GitHub for collaboration and support only Google Firebase for hosting.

It is likely static framework Hugo (with select themes) and perhaps one other static website framework will be supported.

Google firebase hosting updates are very quick and updated websites are ready very quickly.

Namecheap will be recommended for DNS.


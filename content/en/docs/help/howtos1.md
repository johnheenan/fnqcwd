---
title: "Quick Tips"
description: "A disorganised collection of helpful howto information consistent with the goals of the project."
lead: "A disorganised collection of helpful howto information consistent with the goals of the project."
date: 2021-11-06T08:49:31+00:00
lastmod: 2021-11-06T08:49:31+00:00
draft: false
images: []
menu:
  docs:
    parent: "help"
weight: 641
toc: true
---


Quick tips divided into three sections for:
* Cinnamon Linux Mint users, also relevant for other Linux and for macOS users
* VirtualBox users in Windows to run Cinnamon within Windows
* Windows Subsystem for Linux (WSL) users, for which many of the Cinnamon tips are also relevant

- [Quick Tips for Cinnamon](#quick-tips-for-cinnamon)
  - [How to quickly find last few commands to avoid retyping](#how-to-quickly-find-last-few-commands-to-avoid-retyping)
  - [How to stop a command and also cancel a command quickly before executing it](#how-to-stop-a-command-and-also-cancel-a-command-quickly-before-executing-it)
  - [How to find previous commands with reverse search](#how-to-find-previous-commands-with-reverse-search)
  - [How to find previous commands with forward search](#how-to-find-previous-commands-with-forward-search)
  - [How to DIRECTLY generate a html file, and so also pdf file, from a .md markdown file](#how-to-directly-generate-a-html-file-and-so-also-pdf-file-from-a-md-markdown-file)
  - [How to preview a .md (markdown) file in VSCode without a local web server](#how-to-preview-a-md-markdown-file-in-vscode-without-a-local-web-server)
  - [What is ~ and cd?](#what-is--and-cd)
  - [How to generate an image of a portion of a screen. Also enables a lower resolution image to be made of an image and also crop an image](#how-to-generate-an-image-of-a-portion-of-a-screen-also-enables-a-lower-resolution-image-to-be-made-of-an-image-and-also-crop-an-image)
  - [How to quickly access, save and crop photographs on a mobile phone from laptop (Windows or Cinnamon)](#how-to-quickly-access-save-and-crop-photographs-on-a-mobile-phone-from-laptop-windows-or-cinnamon)
  - [How to quickly copy and paste within a Cinnamon Terminal](#how-to-quickly-copy-and-paste-within-a-cinnamon-terminal)
  - [General copy and paste anywhere within Cinnamon](#general-copy-and-paste-anywhere-within-cinnamon)
  - [How to quickly copy and paste within an editor such as `xed` and VSC `code` in Cinnamon](#how-to-quickly-copy-and-paste-within-an-editor-such-as-xed-and-vsc-code-in-cinnamon)
  - [How to quickly undo and redo within an editor such as `xed` and VSC `code` in Cinnamon](#how-to-quickly-undo-and-redo-within-an-editor-such-as-xed-and-vsc-code-in-cinnamon)
  - [Copy and paste within a non gui console shell (if mouse works)](#copy-and-paste-within-a-non-gui-console-shell-if-mouse-works)
  - [How to keep Cinnamon up to date without Update Manager](#how-to-keep-cinnamon-up-to-date-without-update-manager)
  - [How to keep Homebrew (brew) installed software up to date](#how-to-keep-homebrew-brew-installed-software-up-to-date)
  - [How to naviagte desktop workspaces in Cinnamon](#how-to-naviagte-desktop-workspaces-in-cinnamon)
  - [Shifting windows within a desktop workspace in Cinnamon](#shifting-windows-within-a-desktop-workspace-in-cinnamon)
  - [Release keyboard capture in VirtualBox with keyboard](#release-keyboard-capture-in-virtualbox-with-keyboard)
  - [Open VSC (Visual Studio Code) in a directory you are already in, in a shell (Linux or Windows)](#open-vsc-visual-studio-code-in-a-directory-you-are-already-in-in-a-shell-linux-or-windows)
  - [Open File Explorer Nemo in a directory you are already in, in a shell, such as Teminal, in Cinnamon](#open-file-explorer-nemo-in-a-directory-you-are-already-in-in-a-shell-such-as-teminal-in-cinnamon)
  - [Open a shell (Terminal) at a directory in File Explorer Nemo in Cinnamon](#open-a-shell-terminal-at-a-directory-in-file-explorer-nemo-in-cinnamon)
  - [Open a shell (Terminal) with keyboard in Cinnamon](#open-a-shell-terminal-with-keyboard-in-cinnamon)
  - [General autocomplete operation on files in Cinnamon terminal shell, including with editor xed](#general-autocomplete-operation-on-files-in-cinnamon-terminal-shell-including-with-editor-xed)
  - [How to install VSC (Visual Studio Code) on Cinnamon](#how-to-install-vsc-visual-studio-code-on-cinnamon)
  - [How to install VSC (Visual Studio Code) on macOS](#how-to-install-vsc-visual-studio-code-on-macos)
  - [How to install OBS (Open Broadcaster Software) Studio on Cinnamon](#how-to-install-obs-open-broadcaster-software-studio-on-cinnamon)
  - [If you do not use brew then use (and only use) 'sudo -i' with npm if -g (for global) is in the command (does not apply to Windows or if you use nvm).](#if-you-do-not-use-brew-then-use-and-only-use-sudo--i-with-npm-if--g-for-global-is-in-the-command-does-not-apply-to-windows-or-if-you-use-nvm)
- [VirtualBox on Windows](#virtualbox-on-windows)
  - [Obscure error if using older versions of VirtualBox with Windows](#obscure-error-if-using-older-versions-of-virtualbox-with-windows)
- [WSL Quick Tips. Most Cinnamon Tips Above Also Apply](#wsl-quick-tips-most-cinnamon-tips-above-also-apply)
  - [Difference between 'wsl', 'wsl ~' and 'wsl command'](#difference-between-wsl-wsl--and-wsl-command)
  - [The WSL2 file system, the Windows file system and the cross file system latency penalty](#the-wsl2-file-system-the-windows-file-system-and-the-cross-file-system-latency-penalty)
  - [How to determine if a command crosses a file system (and so might be better if rewritten to avoid latency)](#how-to-determine-if-a-command-crosses-a-file-system-and-so-might-be-better-if-rewritten-to-avoid-latency)
  - [How to use File Explorer Nautilus (and other GUIs) with WSL2 on Windows 11](#how-to-use-file-explorer-nautilus-and-other-guis-with-wsl2-on-windows-11)
  - [Why use Nautilus instead of Windows File Explorer with WSL2?](#why-use-nautilus-instead-of-windows-file-explorer-with-wsl2)
  - [How to execute WSL shell commands directly from a Windows shell](#how-to-execute-wsl-shell-commands-directly-from-a-windows-shell)
  - [Backup to archive within WSL2 and copy archive file from WSL2 filesystem to Windows filesystem with four different shells](#backup-to-archive-within-wsl2-and-copy-archive-file-from-wsl2-filesystem-to-windows-filesystem-with-four-different-shells)


## Quick Tips for Cinnamon

### How to quickly find last few commands to avoid retyping

Keep pressing the up arrow key. One press shows last command. Two presses the second last command. Can edit.

### How to stop a command and also cancel a command quickly before executing it
* `Ctrl+C` will cancel a command before execution and give you a new empty command line
* `Ctrl+C` MAY stop a command while executing
* Sometimes several quick `Ctrl+C` MAY stop a command while executing


### How to find previous commands with reverse search
1. `Ctrl+R`
1. Type in a few search letters
1. `Ctrl+R` multiple times to keep going back and find a used command close to what you want
1. `Ctrl+S` to go forward. DO NOT USE UNTIL YOU HAVE SEEN NEXT HINT. Otherwise your terminal will get frozen. Use `Ctrl+Q` to unfreeze ot use `CTRL+C` twice.
1. Use any other keys to cease, navigate, and edit in line if necessary (including enter to accept command as is)
1. Use `Ctrl+C` to cancel quickly

### How to find previous commands with forward search
1. `stty -ixon` required once in shell
1. Can now use `Ctrl+S` in step 4 above to go forward again, after having gone backward with `Ctrl+R`
1. To avoid typing in command `stty -ixon` in shell, add command into to ~/.bashrc.


### How to DIRECTLY generate a html file, and so also pdf file, from a .md markdown file
Suppose you have been editing your .md markdown file in VSCode (`code .` in directory file is in). Suppose, for security reasons, you do not want to risk running the markdown file through hugo or some other such markdown processor because the file contains passwords and you need to avoid accidental publishing.

1. Install VSCode Extension 'Markdown All in One'. This allows a .md file to be saved directly as a .html file
1. With markdown file open: press `F1` or `Ctrl+Shift+P` to open commands
1. Search for 'Markdown All in One: Print current document to HTML' and execute the command with `Enter`
1. Find html document with Nemo or other file explorer and double click to open in browser.
1. From browser: File, Print, Save as pdf.

### How to preview a .md (markdown) file in VSCode without a local web server
`Ctrl+Shift+V` OR right click on Tab of file then click on 'Open Preview'. The preview cannot be converted to a html document. Hence reason for tip above.


### What is ~ and cd?
`cd` is the change directory command. On its own `cd` takes you to your home directory. `~` in a shell is the home directory. The commands `cd` and `cd ~` both have the same effect (change to home directory).


### How to generate an image of a portion of a screen. Also enables a lower resolution image to be made of an image and also crop an image

**In Windows**: Shift + Win  + S, Use mouse + Left Button to draw a rectangle, copy image to clipboard or save as file

**In Cinnamon Linux Mint**: Shift + PrtScn, continue as above


### How to quickly access, save and crop photographs on a mobile phone from laptop (Windows or Cinnamon)

1. Plug phone into laptop with USB cable
2. Unlock screen
3. Go to DCIM directory (for Android)
4. Go to camera directory
5. Double click on any image to view
6. Draw a rectangle over portion of image you want (Shift + PrtScn in Cinnamon)
7. Save image to file

### How to quickly copy and paste within a Cinnamon Terminal

1. Select text within terminal
2. `Shift + Insert` pastes where cursor is


### General copy and paste anywhere within Cinnamon
1. Select text
2. Right Click Mouse, Copy
3. Place cursor, Right Click Mouse, Paste

### How to quickly copy and paste within an editor such as `xed` and VSC `code` in Cinnamon
As above but you can also use `Ctrl + C` for copy and `Ctrl + V` for paste. Also `Ctrl + X` for cut.

### How to quickly undo and redo within an editor such as `xed` and VSC `code` in Cinnamon
Use `Ctrl + Z` for Undo and `Ctrl + Y` for Redo


### Copy and paste within a non gui console shell (if mouse works)
1. Select text
2. Right Click Mouse

### How to keep Cinnamon up to date without Update Manager
```
sudo apt update
sudo apt upgrade
```

### How to keep Homebrew (brew) installed software up to date
To upgrade all software use `brew upgrade`. Note there is no sudo.

If you installed software through brew with `--cask` then also run `brew upgrade --cask`.

You can upgrade individual installations, such as yarn or firebase cli with the same command used to install, such as `brew install firebase-cli`.

### How to naviagte desktop workspaces in Cinnamon

`Ctrl + Alt` with arrow keys


### Shifting windows within a desktop workspace in Cinnamon

`Ctrl + Win` with arrow keys. Note this key combination in Windows with left and right keys shift the desktop workspace in Windows. In Windows the equivalent is `Win` with arrow keys.

### Release keyboard capture in VirtualBox with keyboard
Use the right `Ctrl`


### Open VSC (Visual Studio Code) in a directory you are already in, in a shell (Linux or Windows)
```
code .

In WSL this will still work even without installing VSC within WSL. An extension required will be also be autimatically installed.

Also, within WSL, run `code .` from within a WSL shell.

Do not use 'right click`, `Open eith Code' from Windows FIle Explorer into a WSL file share (\\wsl$\)

```
note the full stop at end.


### Open File Explorer Nemo in a directory you are already in, in a shell, such as Teminal, in Cinnamon
```
nemo .

```
note the full stop at end.



### Open a shell (Terminal) at a directory in File Explorer Nemo in Cinnamon
* Right Click on directory name or a space within a directory
* Click on 'Open in Terminal'


### Open a shell (Terminal) with keyboard in Cinnamon

`Ctrl + Alt + T`




### General autocomplete operation on files in Cinnamon terminal shell, including with editor xed
Normally Linux shell requires the directory to be specified as part of file name to confine autocomplete of filenames in a directory with `Tab`.

For example, suppose we want to operate on file with filename 'filename_a' in current directory with editor `xed`. Suppose no other files in directory start with name 'fi'

Normally in Linux shells `xed ./fi + Tab` is required for autocompete to get the complete name `xed ./filename_a`

In Cinnamon Terminal the './' can be left out. So `xed fi + Tab` is sufficient to get the complete name `xed ./filename_a`

Also if you get no matches with a `+ Tab`, then `+ Tab Tab` will show you multiple matches, so you can add more letters and try `+ Tab` again.



### How to install VSC (Visual Studio Code) on Cinnamon

On macOS `brew install --cask visual-studio-code` may work

Copy and paste:
```
cd
sudo apt update
sudo apt install apt-transport-https
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sudo install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
rm microsoft.gpg
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
sudo apt update
sudo apt install code
```
Start with command `code`

### How to install VSC (Visual Studio Code) on macOS

On macOS `brew install --cask visual-studio-code` may work


### How to install OBS (Open Broadcaster Software) Studio on Cinnamon

From <https://obsproject.com/wiki/install-instructions#ubuntumint-installation>

Does not work with VirtualBox

Copy and paste:
```
sudo apt update
sudo apt install ffmpeg
sudo apt install v4l2loopback-dkms
sudo add-apt-repository ppa:obsproject/obs-studio
sudo apt update
sudo apt install obs-studio
```
Start with command `obs`

Yellow transparent circle around pointer cursors are commonly used with OBS presentations.  By far the easiest way to achieve this is to modify the cursor itself, as it avoids many capture issues.

For use of OBS with Linux, a yellow transparent circle around pointer cursor package file is available from ycDmz at <https://www.gnome-look.org/p/999856/>. The ycDMZ folder needs to be copied into /usr/share/icons and  'System Settings', 'Themes', 'Mouse Pointer', 'ycDMZ' needs to be chosen.

For use of OBS with Windows, there are numerous .ico files to choose from.

No other approach works out of the box with OBS and no other approach lacks substantial frustrating issues.

### If you do not use brew then use (and only use) 'sudo -i' with npm if -g (for global) is in the command (does not apply to Windows or if you use nvm).

Does not apply if you have used brew to install node/npm or install firebase-cli or if you are using nvm

There is an example of use in [Basic Intallation]({{< relref "basic-installation/#nodenpm" >}})

Firebase-tools occasionally advises upgrades to itself and other packages. Npm makes install and updates very easy. So if '-g' is part of advice, prepend the advised command with 'sudo -i'. If '-g' is not part of advice, then do not prepend 'sudo -i'

Make sure '-i' is added to 'sudo' to allow the correct environment for install or update.

An npm command with 'i' or 'install' also updates.


## VirtualBox on Windows

If you normally run Windows but intend supporting users who will only run Cinnamon then using VirualBox is helpful to run an instance of Cinnamon within Windows.

### Obscure error if using older versions of VirtualBox with Windows
**If you are using VirtualBox 6.1.30 or above this does not apply**

**If you are using VirtualBox 6.1.28** and WSL and cannot get VirtualBox to boot your VMs due to
`"...Call to NEMR0InitVMPart2 failed: VERR_NEM_INIT_FAILED (VERR_NEM_VM_CREATE_FAILED)..."`
please see <https://forums.virtualbox.org/viewtopic.php?f=6&t=104175&start=15>

Solution: turn off hyperv
1. open a cmd with admin privilige
1. `bcdedit.exe /set hypervisorlauchtype off`
1. reboot

When running WSL:
`bcdedit /set hypervisorlaunchtype auto`
When running Visualbox:
`bcdedit /set hypervisorlaunchtype off`


**If you are using VirtualBox 6.1.26** then you cannot use
```
apt update
apt upgrade
```
as this command will attempt to install the MESA upgrade that a fresh install of Linux Mint 20.2 wants




## WSL Quick Tips. Most Cinnamon Tips Above Also Apply

Most relevant for those who use Debian and Ubuntu distributions for WSL2 for Windows 11.

### Difference between 'wsl', 'wsl ~' and 'wsl command'

The wsl command is specific to windows and is normally run from a PS (PowerShell) or CMD shell.

`wsl` on its own starts a wsl interactive shell (normally bash) in the Windows directory launched from. Handy to run commands such as `grep text file` in the bash shell to search for text within file.

`wsl ~`on its own starts an interactive shell in the wsl home directory, not the windows directory launched from

Type `exit` to leave WSL bash shell and renter windows shell.

`wsl cmd` enters bash shell, runs command and exits back to Windows,

For example `wsl grep text file` will search for text within file of the windows directory currently in.

However `wsl grep text ~/file` (note the extra ~/) will search for text within file of the WSL home directory.

### The WSL2 file system, the Windows file system and the cross file system latency penalty

The WSL2 file system can be accessed from Windows through the `\\wsl$\` file share and can be mapped to a network drive.

Within wsl, windows drives, such as `C:`, are accessed as `/mnt/c`.

There is a severe latency penalty running WSL2 commands on Windows file system files, as also running windows commands on WSL file system files.

Please see archive example below for an example to minimise this impact. Copying an already packaged archived file across file systems involves far less file system operations than creating the packaged archived file, no matter what file system it was created within.


### How to determine if a command crosses a file system (and so might be better if rewritten to avoid latency)

This is applicable to WSL2, not WSL1.

Note to execute Windows commands in wsl you need the full file name of the command, so if the command includes `.exe` in its name this must be included.

1. If you pass a parameter to a wsl command with `/mnt/x` in it, where x represents a Windows drive, then you are crossing.
1. If you pass a parameter to a windows command with `\\wsl$\` (or with a mapping with same), then you are crossing.
1. If you execute any command that includes files or directories from one file system to another, for example creating a packaged archive of files from one file system to another, then you are crossing.

For the last example, it is far more efficient to package within the same file system and then copy the finished package across. There is an example below.

Paradoxically, executing a windows command within wsl that does not include `\\wsl$\` does not necessarily cross. For example within wsl `notepad.exe 'C:\Users\john\hello.txt'` does not cross. However the system environment used by such Windows commands within wsl is really not appropriate, so you need a good reason.

For illustrative purposes, executing from wsl `notepad.exe /mnt/c/Users/john/hello.txt` is incorrect and will not work. If you do startup a Windows command that includes `.exe`, it is going to expect to use a name consistent with rules recognised by normal Windows. VSC is an exception. So is the MINGW64/MYS2 shell, but it parses and corrects the path of arguments, so it is not the same.


### How to use File Explorer Nautilus (and other GUIs) with WSL2 on Windows 11


Have a look at <https://docs.microsoft.com/en-us/windows/wsl/tutorials/gui-apps>

There is no need to install VSC (Visual Studio Code) under WSL2. Running `code .` in a WSL shell will open its VSC GUI part in Windows but will interact with WSL using WSL commands through a network extension, including visible terminal (or shell) commands.

### Why use Nautilus instead of Windows File Explorer with WSL2?

If you prefer to move around large files and directories within WSL2 with a GUI, rather than a shell, then Nautilus avoids a severe and well known latency issue between Windows and WSL with regards to files.

So instead of `explorer.exe .` in a WSL2 shell use `nautilus .` in the WSL2 shell. Please see above tip for more information.


### How to execute WSL shell commands directly from a Windows shell

Contrast entering interactive WSL shell
```bash
PS C:\Users\john> wsl ~           # append ~ to start interactive shell in WSL home directory
john@DESKTOP-JOHN:~$ ls -l hello  # show permissions of hello that were changed with command chmod 550 hello
-r-xr-x--- 1 john john 21 Jan 31 14:05 hello
john@DESKTOP-JOHN:~$ cat hello    # show contents of hello executable file
#!/bin/sh
echo hello                        # send 'hello' to standard output
john@DESKTOP-JOHN:~$ ./hello      # execute script file hello
hello
john@DESKTOP-JOHN:~$
```

with direct execution of WSL commands from a Windows shell without entering interactive WSL shell

```powershell
PS C:\Users\john> wsl ls -l ~/hello
-r-xr-x--- 1 john john 21 Jan 31 14:05 /home/john/hello
PS C:\Users\john> wsl cat ~/hello
#!/bin/sh
echo hello
PS C:\Users\john> wsl ~/hello
hello
PS C:\Users\john>
```

Please see a more practical example below.


### Backup to archive within WSL2 and copy archive file from WSL2 filesystem to Windows filesystem with four different shells
This incomplete example avoids inefficiently creating an archive across file system boundaries of WSL2 and Windows. See above also

Enter a WSL shell with `wsl ~` and copy and paste:


```bash
cat <<EOF > ~/wsl_home_backup
bufile=wsl_home_backup.tgz
cd ~
tar -zcvf \$bufile --exclude=\$bufile --exclude=./.* --exclude=.git --exclude=node_modules --exclude=*.log --exclude=generated --exclude=target --exclude=cache  . .bashrc .profile .git*
EOF

chmod 770 ~/wsl_home_backup

```
Remove \ before $ above (twice) if you do not use `cat <<EOF`.

Following are four simple examples of controlling the backup, with three different shells from Windows and one WSL shell. These commands can be placed into script files and made more robust by testing for existence of ~/wsl_home_backup file and for the backup directory as well as with further variable use.

Using Powershell:
```powershell
wsl ~/wsl_home_backup
Copy-Item -Path \\wsl$\Debian\home\john\wsl_home_backup.tgz -Destination D:\backups\wsl

```

Using CMD:
```cmd
wsl ~/wsl_home_backup
powershell Copy-Item -Path \\wsl$\Debian\home\john\wsl_home_backup.tgz -Destination D:\backups\wsl

```

Using MINGW64:
```mingw64
powershell 'wsl ~/wsl_home_backup'
powershell 'Copy-Item -Path \\wsl$\Debian\home\john\wsl_home_backup.tgz -Destination D:\backups\wsl'
```

Using WSL:
```bash
~/wsl_home_backup
powershell.exe 'Copy-Item -Path \\wsl$\Debian\home\john\wsl_home_backup.tgz -Destination D:\backups\wsl'
```




